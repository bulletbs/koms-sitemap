<?php defined('SYSPATH') or die('No direct script access.');

/**
 * MInion task for sitemap generation
 */
class Task_Sitemap extends Minion_Task
{
    const SITEMAP_LINKS_LIMIT = 10000;
    const SITEMAP_FOLDER = "media/upload/sitemap/";
    const SITEMAP_INDEX_FOLDER = "";

    const GZIP = true;

    public $skip_auto_render = array(
        'generate',
    );

    /**
     * Generate sitemaps
     */
    protected function _execute(Array $params){
        $start = time();
        set_time_limit(3600);

        Kohana::$environment = !isset($_SERVER['windir']) && !isset($_SERVER['GNOME_DESKTOP_SESSION_ID']) ? Kohana::PRODUCTION : Kohana::DEVELOPMENT;

        $global = Kohana::$config->load('global')->as_array();
        $_SERVER['HTTP_HOST'] = $global['project']['host'];

        $config = Kohana::$config->load('module_sitemap')->as_array();
        $sitemap_index = array();

        foreach($config as $module_id => $module){
            $links = array();
            $_priority = isset($module['priority']) ? $module['priority'] : '0.5';
            $_frequency = isset($module['frequency']) ? $module['frequency'] : 'weekly';
            if(!isset($module['sources']) || !count($module['sources'])){
	            print 'Module '.$module['name'].'('.$module_id.') have no sources'.PHP_EOL;
	            continue;
            }
	        print 'Module '.$module['name'].'('.$module_id.') have '.count($module['sources']).' sources'.PHP_EOL;

            foreach($module['sources'] as $_source){
                if(isset($_source['return'])){
                    if($_source['return'] == 'sitemaps'){
                        foreach(ORM::factory($_source['model'])->{$_source['get_links_method']}($_source) as $_sitemap)
                            $sitemap_index[] = $_sitemap;
                    }
                    elseif($_source['return'] == 'links'){
                        $links[] = array(
                            'links' => $_source['links'],
                            'priority' => isset($_source['priority']) ? $_source['priority'] : $_priority,
                            'frequency' => isset($_source['frequency']) ? $_source['frequency'] : $_frequency,
                        );
                    }
                }
                else
                    $links[] = array(
                        'links' => ORM::factory($_source['model'])->{$_source['get_links_method']}(),
                        'priority' => isset($_source['priority']) ? $_source['priority'] : $_priority,
                        'frequency' => isset($_source['frequency']) ? $_source['frequency'] : $_frequency,
                    );
            }
            $sitemap = new Sitemap();
            $sitemap->gzip = Task_Sitemap::GZIP;
            $url = new Sitemap_URL;
            $file = DOCROOT . self::SITEMAP_FOLDER . $module['name'] .".xml.gz";
            $sitemap_link = URL::base(KoMS::protocol()). self::SITEMAP_FOLDER . $module['name'] .".xml.gz";
            foreach($links as $_link_data){
                if(isset($_link_data['priority']))
                    $_priority = $_link_data['priority'];
                if(isset($_link_data['frequency']))
                    $_frequency= $_link_data['frequency'];
                foreach($_link_data['links'] as $_link_id => $_link){
                    $url->set_loc(URL::base(KoMS::protocol()).$_link)
                        ->set_last_mod(time())
                        ->set_change_frequency($_frequency)
                        ->set_priority($_priority);
                    $sitemap->add($url);

                    /* При достижении лимита открываем следующий файл */
                    if($_link_id>0 && $_link_id % self::SITEMAP_LINKS_LIMIT == 0){
//                    $response = urldecode($sitemap->render());
                        $response = $sitemap->render();
                        file_put_contents($file, $response);
                        $sitemap_index[] = $sitemap_link;
                        unset($sitemap);
                        unset($response);

                        $sitemap = new Sitemap();
                        $sitemap->gzip = Task_Sitemap::GZIP;
                        $file = DOCROOT . self::SITEMAP_FOLDER . $module['name'] ."_". ($_link_id / self::SITEMAP_LINKS_LIMIT + 1) .".xml.gz";
                        $sitemap_link = URL::base(KoMS::protocol()). self::SITEMAP_FOLDER . $module['name'] ."_". ($_link_id / self::SITEMAP_LINKS_LIMIT + 1) .".xml.gz";
                    }
                }
            }
//            $response = urldecode($sitemap->render());
            $response = $sitemap->render();
            file_put_contents($file, $response);
            $sitemap_index[] = $sitemap_link;
            unset($sitemap);
            unset($response);
        }
        $index = new SitemapIndex();
        foreach($sitemap_index as $_link){
            $sitemap = new SitemapIndex_URL();
            $sitemap->set_loc($_link)->set_last_mod(time());
            $index->add($sitemap);
        }
        $file = DOCROOT . self::SITEMAP_INDEX_FOLDER . 'sitemap.xml';
//        $response = urldecode($index->render());
        $response = $index->render();
        file_put_contents($file, $response);
//        Minion_CLI::write(Debug::dump($sitemap_index));
        print 'Operation taken '. (time() - $start) .' seconds'.PHP_EOL;
    }
}
